import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import { loadAuthors } from "./store/authors";
import { loadCategories } from "./store/categories";

import Home from "./pages/Home";
import AddVideo from "./pages/AddVideo";
import EditVideo from "./pages/EditVideo";

import { AppBar, Toolbar, Typography, Button } from "@material-ui/core";

const App: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadAuthors());
    dispatch(loadCategories());
  }, [dispatch]);

  return (
    <>
      <Router>
        <AppBar position="static">
          <StyleToolbar>
            <StyledLink to="/">
              <Typography variant="h6">Videos</Typography>
            </StyledLink>
            <StyledLink to="/add-video">
              <AddButton variant="contained">Add video</AddButton>
            </StyledLink>
          </StyleToolbar>
        </AppBar>
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/add-video">
            <AddVideo />
          </Route>
          <Route path="/edit-video">
            <EditVideo />
          </Route>
        </Switch>
      </Router>
    </>
  );
};

export default App;

const StyleToolbar = styled(Toolbar)`
  display: flex;
  justify-content: space-between;
  background: #353b3f;
`;

const AddButton = styled(Button)`
  background: #34a846;
  color: white;
`;

const StyledLink = styled(Link)`
  color: inherit;
  text-decoration: none;
`;
