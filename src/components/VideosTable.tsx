import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Button,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import type { RootState } from "../store";
import videosSlice from "../store/videos";
import { videosFormat } from "../utils/videosFormat";

import { DeleteVideoButton } from "./DeleteVideoButton";

const { editedVideo } = videosSlice.actions;

export const VideosTable: React.FC = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const categories = useSelector(
    (state: RootState) => state.categories.categories
  );
  const authors = useSelector((state: RootState) => state.authors.authors);
  const searchedVideo = useSelector(
    (state: RootState) => state.videos.searchedVideo
  );

  const videos = videosFormat(categories, authors);

  if (videos.length === 0) return null;

  const handleEditClick = async (authorId: number, videoId: number) => {
    await dispatch(editedVideo({ authorId, videoId }));
    history.push("/edit-video");
  };

  return (
    <TableContainer component={Paper} style={{ marginTop: "40px" }}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>Video Name</TableCell>
            <TableCell>Author</TableCell>
            <TableCell>Category name</TableCell>
            <TableCell>Highest quality format</TableCell>
            <TableCell>Release date</TableCell>
            <TableCell>Options</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {videos
            .filter((video) =>
              searchedVideo !== ""
                ? video.videoName
                    .toLowerCase()
                    .includes(searchedVideo.toLowerCase())
                : true
            )
            .map((video) => (
              <TableRow key={video.id} className={classes.root}>
                <TableCell component="th" scope="row">
                  {video.videoName}
                </TableCell>
                <TableCell>{video.authorName}</TableCell>
                <TableCell>{video.categoryName}</TableCell>
                <TableCell>{video.highestQualityFormat}</TableCell>
                <TableCell>{video.releaseDate}</TableCell>
                <TableCell>
                  <Button
                    variant="contained"
                    size="small"
                    onClick={() => handleEditClick(video.authorId, video.id)}
                    style={{
                      marginRight: 8,
                      background: "#35a2b8",
                      color: "white",
                    }}
                  >
                    Edit
                  </Button>
                  <DeleteVideoButton
                    authorId={video.authorId}
                    videoId={video.id}
                  />
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}));
