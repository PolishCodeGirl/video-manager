import React from "react";

import { Container } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";

interface PageProps {
  title: string;
  children: React.ReactNode;
}

const Page: React.FC<PageProps> = ({ title, children }) => {
  return (
    <Container>
      <Typography variant="h4" style={{ margin: "16px 0" }}>
        {title}
      </Typography>
      {children}
    </Container>
  );
};

export default Page;
