import React, { useState } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";

import { deleteVideo } from "../store/videoThunks";

import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";

interface Props {
  authorId: number;
  videoId: number;
}

export const DeleteVideoButton: React.FC<Props> = ({ authorId, videoId }) => {
  const dispatch = useDispatch();
  const [isDialogVisible, setIsDialogVisible] = useState(false);

  const showDialog = () => {
    setIsDialogVisible(true);
  };

  const hideDialog = () => {
    setIsDialogVisible(false);
  };

  const handleDeleteVideo = () => {
    dispatch(deleteVideo({ authorId, videoId }));
    hideDialog();
  };

  return (
    <>
      <DeleteButton variant="contained" size="small" onClick={showDialog}>
        Delete
      </DeleteButton>

      <Dialog open={isDialogVisible} onClose={hideDialog}>
        <Wrapper>
          <Typography variant="body1">
            Are you sure, you want to delete video?
          </Typography>

          <ButtonWrapper>
            <ConfirmButton
              variant="contained"
              size="small"
              onClick={handleDeleteVideo}
            >
              Delete
            </ConfirmButton>

            <CancelButton variant="contained" size="small" onClick={hideDialog}>
              Cancel
            </CancelButton>
          </ButtonWrapper>
        </Wrapper>
      </Dialog>
    </>
  );
};

const Wrapper = styled.div`
  padding: 25px;
`;

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
  margin-top: 15px;
`;

const DeleteButton = styled(Button)`
  background: #dd3444;
  color: white;
`;

const ConfirmButton = styled(Button)`
  background: #317bfe;
  color: white;
`;

const CancelButton = styled(Button)`
  background: #f8f9fa;
`;
