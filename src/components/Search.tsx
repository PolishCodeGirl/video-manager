import React, { useState } from "react";
import { useDispatch } from "react-redux";

import videosSlice from "../store/videos";

import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

const { searchedVideo } = videosSlice.actions;

const Search = () => {
  const dispatch = useDispatch();
  const [searchedValue, setSearchedValue] = useState("");

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.value === "") {
      dispatch(searchedVideo(event.target.value));
    }

    setSearchedValue(event.target.value);
  };

  const handleSearch = () => {
    dispatch(searchedVideo(searchedValue));
  };

  return (
    <div style={{ display: "flex" }}>
      <TextField
        id="video-search"
        placeholder="Search video names"
        variant="outlined"
        size="small"
        value={searchedValue}
        onChange={handleChange}
      />

      <Button
        variant="contained"
        size="small"
        style={{ background: "#317bfe", color: "white" }}
        onClick={handleSearch}
      >
        Search
      </Button>
    </div>
  );
};

export default Search;
