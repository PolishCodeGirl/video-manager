import React, { useState } from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";

import type { RootState } from "../store";
import { VideoFormPayload } from "../common/interfaces";

import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Dialog from "@material-ui/core/Dialog";

interface VideoFormProps {
  videoName?: string;
  authorId?: number | null;
  categoryIds?: string[];
  onSubmit: (data: VideoFormPayload) => void;
  onCancel?: () => void;
}

const VideoForm = (props: VideoFormProps) => {
  const [name, setName] = useState(props.videoName || "");
  const [authorId, setAuthorId] = useState(props.authorId || "");
  const [categoryIds, setCategoryIds] = useState(props.categoryIds || []);
  const [isDialogVisible, setIsDialogVisible] = useState(false);

  const isFormValid = name !== "" && authorId !== "" && categoryIds.length > 0;

  const videoCategories = useSelector(
    (state: RootState) => state.categories.categories
  );
  const videoAuthors = useSelector((state: RootState) => state.authors.authors);

  const showDialog = () => {
    setIsDialogVisible(true);
  };

  const hideDialog = () => {
    setIsDialogVisible(false);
  };

  const handleChangeMultiple = (
    event: React.ChangeEvent<{ value: unknown }>
  ) => {
    const { options } = event.target as HTMLSelectElement;
    const value: string[] = [];
    for (let i = 0, l = options.length; i < l; i += 1) {
      if (options[i].selected) {
        value.push(options[i].value);
      }
    }
    setCategoryIds(value);
  };

  const handleOnSubmit = () => {
    if (isFormValid) {
      props.onSubmit({
        videoName: name,
        authorId: Number(authorId),
        categoryIds: categoryIds.map((i) => Number(i)),
      });
    } else {
      showDialog();
    }
  };

  return (
    <From>
      <Wrapper>
        <Typography variant="body1" style={{ width: "15%" }}>
          Video name
        </Typography>
        <TextField
          id="video-name"
          placeholder="Video name"
          variant="outlined"
          value={name}
          onChange={(event: React.ChangeEvent<{ value: unknown }>) =>
            setName(event.target.value as string)
          }
          style={{ width: "85%" }}
        />
      </Wrapper>

      <Wrapper>
        <Typography variant="body1" style={{ width: "15%" }}>
          Video author
        </Typography>
        <Select
          id="author-name"
          value={authorId}
          onChange={(event: React.ChangeEvent<{ value: unknown }>) =>
            setAuthorId(event.target.value as string)
          }
          variant="outlined"
          style={{ width: "85%" }}
        >
          {videoAuthors.map((videoAuthor) => (
            <MenuItem value={videoAuthor.id} key={videoAuthor.id}>
              {videoAuthor.name}
            </MenuItem>
          ))}
        </Select>
      </Wrapper>

      <Wrapper>
        <Typography variant="body1" style={{ width: "15%" }}>
          Video category
        </Typography>

        <Select
          id="video-category"
          value={categoryIds}
          onChange={handleChangeMultiple}
          label="Video categories"
          variant="outlined"
          multiple
          native
          style={{ width: "85%" }}
        >
          {videoCategories.map((videoCategory) => (
            <option value={videoCategory.id} key={videoCategory.id}>
              {videoCategory.name}
            </option>
          ))}
        </Select>
      </Wrapper>

      <Wrapper>
        <div style={{ width: "15%" }} />
        <div style={{ width: "85%" }}>
          <Button
            variant="contained"
            onClick={handleOnSubmit}
            style={{ background: "#317bfe", color: "white", marginRight: 8 }}
          >
            Submit
          </Button>
          <Button
            variant="contained"
            onClick={props.onCancel}
            style={{ background: "#f8f9fa" }}
          >
            Cancel
          </Button>
        </div>
      </Wrapper>

      <Dialog open={isDialogVisible} onClose={hideDialog}>
        <DialogWrapper>
          <Typography variant="body1" style={{ textAlign: "center" }}>
            All fields have to be filled
          </Typography>
        </DialogWrapper>
      </Dialog>
    </From>
  );
};

export default VideoForm;

const From = styled.form`
  display: flex;
  flex-direction: column;
`;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  margin-top: 10px;
`;

const DialogWrapper = styled.div`
  width: 200px;
  padding: 25px;
`;
