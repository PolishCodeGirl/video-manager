import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";

import authorsSlice from "./store/authors";
import categoriesSlice from "./store/categories";
import videosSlice from "./store/videos";

const reducer = combineReducers({
  authors: authorsSlice.reducer,
  categories: categoriesSlice.reducer,
  videos: videosSlice.reducer,
});

const store = configureStore({
  reducer,
});

export default store;
export type RootState = ReturnType<typeof reducer>;
