import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import { Category } from "../common/interfaces";
import { getCategories } from "../services/categories";
import { RootState } from "../store";

interface CategoriesState {
  categories: Category[];
  error: string | null;
  isPending: boolean;
}

const categoriesInitialState: CategoriesState = {
  categories: [],
  error: null,
  isPending: false,
};

export const loadCategories = createAsyncThunk<
  any,
  undefined,
  { state: RootState }
>("loadCategories", getCategories);

const categoriesSlice = createSlice({
  name: "categories",
  initialState: categoriesInitialState,
  reducers: {
    clearErrors(state) {
      state.error = null;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadCategories.pending, (state) => {
        state.isPending = true;
        state.error = null;
      })
      .addCase(loadCategories.fulfilled, (state, action) => {
        state.categories = action.payload;
        state.isPending = false;
      })
      .addCase(loadCategories.rejected, (state, action) => {
        state.isPending = false;
        state.error = `Categories ${action.error.name}: ${action.error.message}`;
      });
  },
});

export default categoriesSlice;
