import { createAsyncThunk } from "@reduxjs/toolkit";

import type { AuthorVideoType, VideoFormPayload } from "../common/interfaces";

import {
  VideoModel,
  addVideoToAuthor,
  editVideoInAuthor,
  removeVideoFromAuthor,
} from "../models";

import { editAuthor } from "../services/authors";

import { RootState } from "../store";

export const addVideo = createAsyncThunk<
  any,
  VideoFormPayload,
  { state: RootState }
>(
  "addVideo",
  ({ authorId, videoName, categoryIds }, { getState, dispatch }) => {
    const authors = getState().authors.authors;
    const authorToEdit = authors.find(({ id }) => id === authorId)!;
    const newVideo = VideoModel(videoName, categoryIds);
    const authorWithAddedVideo = addVideoToAuthor(authorToEdit, newVideo);

    dispatch({ type: "authors/editAuthor", payload: authorWithAddedVideo });

    return editAuthor(authorWithAddedVideo);
  }
);

export const editVideo = createAsyncThunk<
  any,
  {
    authorId: number;
    videoId: number;
    videoName: string;
    categoryIds: number[];
    previousAuthorId: number;
  },
  { state: RootState }
>(
  "editVideo",
  (
    { authorId, videoId, videoName, categoryIds, previousAuthorId },
    { getState, dispatch }
  ) => {
    const authors = getState().authors.authors;

    if (authorId !== previousAuthorId) {
      const newVideo = VideoModel(videoName, categoryIds);

      const previousAuthor = authors.find(({ id }) => id === previousAuthorId)!;
      const previousAuthorWithRemovedVideo = removeVideoFromAuthor(
        previousAuthor,
        videoId
      );

      const newAuthor = authors.find(({ id }) => id === authorId)!;
      const newAuthorWithAddedVideo = addVideoToAuthor(newAuthor, newVideo);

      dispatch({
        type: "authors/editAuthor",
        payload: previousAuthorWithRemovedVideo,
      });
      dispatch({
        type: "authors/editAuthor",
        payload: newAuthorWithAddedVideo,
      });

      return Promise.all([
        editAuthor(previousAuthorWithRemovedVideo),
        editAuthor(newAuthorWithAddedVideo),
      ]);
    }

    const authorToEdit = authors.find(({ id }) => id === authorId)!;
    const oldVideo = authorToEdit.videos.find(({ id }) => id === videoId)!;

    const editedVideo: AuthorVideoType = {
      ...oldVideo,
      name: videoName,
      catIds: categoryIds,
    };

    const editedAuthor = editVideoInAuthor(authorToEdit, editedVideo);

    dispatch({ type: "authors/editAuthor", payload: editedAuthor });

    return editAuthor(editedAuthor);
  }
);

export const deleteVideo = createAsyncThunk<
  any,
  { authorId: number; videoId: number },
  { state: RootState }
>("deleteVideo", ({ authorId, videoId }, { getState, dispatch }) => {
  const authors = getState().authors.authors;
  const authorToEdit = authors.find((author) => author.id === authorId);

  if (typeof authorToEdit === "undefined")
    throw new Error("Cannot find author");

  const authorWithoutVideo = removeVideoFromAuthor(authorToEdit, videoId);

  dispatch({ type: "authors/editAuthor", payload: authorWithoutVideo });

  return editAuthor(authorWithoutVideo);
});
