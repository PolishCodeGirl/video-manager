import { createSlice } from "@reduxjs/toolkit";

import { addVideo, editVideo, deleteVideo } from "./videoThunks";

interface VideosState {
  error: string | null;
  isPending: boolean;
  editedVideo: {
    authorId: number | null;
    videoId: number | null;
  };
  searchedVideo: string;
}

const videosInitialState: VideosState = {
  error: null,
  isPending: false,
  editedVideo: {
    authorId: null,
    videoId: null,
  },
  searchedVideo: "",
};

const pendingActionHandler = (state: VideosState) => {
  state.isPending = true;
  state.error = null;
};

const fulfilledActionHandler = (state: VideosState) => {
  state.isPending = false;
};

const rejectedActionHandler = (state: VideosState, action: any) => {
  state.isPending = false;
  state.error = `Videos ${action.error.name}: ${action.error.message}`;
};

const videosSlice = createSlice({
  name: "videos",
  initialState: videosInitialState,
  reducers: {
    searchedVideo(state, action) {
      state.searchedVideo = action.payload;
    },
    editedVideo(state, action) {
      state.editedVideo.authorId = action.payload.authorId;
      state.editedVideo.videoId = action.payload.videoId;
    },
    clearEditedVideo(state) {
      state.editedVideo.authorId = null;
      state.editedVideo.videoId = null;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(addVideo.pending, pendingActionHandler)
      .addCase(addVideo.fulfilled, fulfilledActionHandler)
      .addCase(addVideo.rejected, rejectedActionHandler)
      .addCase(editVideo.pending, pendingActionHandler)
      .addCase(editVideo.fulfilled, fulfilledActionHandler)
      .addCase(editVideo.rejected, rejectedActionHandler)
      .addCase(deleteVideo.pending, pendingActionHandler)
      .addCase(deleteVideo.fulfilled, fulfilledActionHandler)
      .addCase(deleteVideo.rejected, rejectedActionHandler);
  },
});

export default videosSlice;
