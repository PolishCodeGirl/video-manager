import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

import { Author } from "../common/interfaces";
import { getAuthors } from "../services/authors";
import { RootState } from "../store";

interface AuthorsState {
  authors: Author[];
  error: string | null;
  isPending: boolean;
}

const authorsInitialState: AuthorsState = {
  authors: [],
  error: null,
  isPending: false,
};

export const loadAuthors = createAsyncThunk<
  any,
  undefined,
  { state: RootState }
>("loadAuthors", getAuthors);

const authorsSlice = createSlice({
  name: "authors",
  initialState: authorsInitialState,
  reducers: {
    editAuthor(state, action) {
      const authorIndex = state.authors.findIndex(
        ({ id }) => id === action.payload.id
      );
      state.authors[authorIndex] = action.payload;
    },
    clearErrors(state) {
      state.error = null;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadAuthors.pending, (state) => {
        state.isPending = true;
        state.error = null;
      })
      .addCase(loadAuthors.fulfilled, (state, action) => {
        state.authors = action.payload;
        state.isPending = false;
      })
      .addCase(loadAuthors.rejected, (state, action) => {
        state.isPending = false;
        state.error = `Authors ${action.error.name}: ${action.error.message}`;
      });
  },
});
export default authorsSlice;
