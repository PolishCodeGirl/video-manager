import { Author, Category, VideoType } from "../common/interfaces";

export function videosFormat(
  categories: Category[] = [],
  authors: Author[] = []
) {
  const videos: VideoType[] = [];

  authors.forEach((author) => {
    author.videos.forEach((video) => {
      const categoryNames = video.catIds.reduce<Category["name"][]>(
        (allCategoryNames, currentCategoryId) => {
          const category = categories.find(
            (category) => category.id === currentCategoryId
          );
          if (category) return [...allCategoryNames, category.name];
          return allCategoryNames;
        },
        []
      );

      const highestQualityFormat = Object.entries(video.formats)
        .map(([type, format]) => ({
          type,
          res: parseInt(format.res),
          size: format.size,
        }))
        // Sort by `res`, then by `size`
        .sort((a, b) => {
          if (a.res === b.res) return b.size > a.size ? 1 : -1;
          else if (a.res > b.res) return -1;
          return 1;
        })[0];

      videos.push({
        id: video.id,
        videoName: video.name,
        authorId: author.id,
        authorName: author.name,
        categoryName: categoryNames.join(", "),
        highestQualityFormat: `${highestQualityFormat.type} ${highestQualityFormat.res}p`,
        releaseDate: new Date(video.releaseDate).toLocaleDateString(),
      });
    });
  });

  return videos;
}
