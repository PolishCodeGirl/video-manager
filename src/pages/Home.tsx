import React from "react";

import Page from "../components/Page";

import { VideosTable } from "../components/VideosTable";
import Search from "../components/Search";

const Home: React.FC = () => {
  return (
    <Page title="VManager Demo v0.0.1">
      <Search />
      <VideosTable />
    </Page>
  );
};

export default Home;
