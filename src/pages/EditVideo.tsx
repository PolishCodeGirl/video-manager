import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

import Page from "../components/Page";
import VideoForm from "../components/VideoForm";
import { VideoFormPayload } from "../common/interfaces";
import type { RootState } from "../store";
import { editVideo } from "../store/videoThunks";

import Divider from "@material-ui/core/Divider";

const EditVideo: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const editedVideos = useSelector(
    (state: RootState) => state.videos.editedVideo
  );
  const authors = useSelector((state: RootState) => state.authors.authors);

  if (!editedVideos.videoId) {
    history.push("/");
    return null;
  }

  const videoAuthor = authors.find(({ id }) => id === editedVideos.authorId)!;
  const videoToEdit = videoAuthor.videos.find(
    ({ id }) => id === editedVideos.videoId
  )!;

  const cancelEditing = () => {
    history.push("/");
  };

  const handleSubmit = (video: VideoFormPayload) => {
    dispatch(
      editVideo({
        videoId: editedVideos.videoId!,
        authorId: video.authorId,
        videoName: video.videoName,
        categoryIds: video.categoryIds,
        previousAuthorId: editedVideos.authorId!,
      })
    );
    history.push("/");
  };

  return (
    <Page title={`Edit video: ${videoToEdit.name}`}>
      <Divider />
      <VideoForm
        videoName={videoToEdit.name}
        authorId={editedVideos.authorId}
        categoryIds={videoToEdit.catIds.map((i) => String(i))}
        onSubmit={handleSubmit}
        onCancel={cancelEditing}
      />
    </Page>
  );
};

export default EditVideo;
