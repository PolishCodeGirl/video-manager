import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

import Page from "../components/Page";
import VideoForm from "../components/VideoForm";
import { VideoFormPayload } from "../common/interfaces";
import { addVideo } from "../store/videoThunks";

import Divider from "@material-ui/core/Divider";

const AddVideo: React.FC = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const cancelAddVideo = () => {
    history.push("/");
  };

  const handleOnSubmit = (newVideo: VideoFormPayload) => {
    dispatch(addVideo(newVideo));
    history.push("/");
  };

  return (
    <Page title="Add video">
      <Divider />
      <VideoForm onSubmit={handleOnSubmit} onCancel={cancelAddVideo} />
    </Page>
  );
};

export default AddVideo;
