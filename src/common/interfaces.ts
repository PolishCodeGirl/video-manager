export interface Category {
  id: number;
  name: string;
}

export type ResolutionType = "720p" | "1080p";

export interface AuthorVideoType {
  id: number;
  catIds: number[];
  name: string;
  formats: {
    [key: string]: { res: ResolutionType; size: number };
  };
  releaseDate: string;
}

export interface Author {
  id: number;
  name: string;
  videos: AuthorVideoType[];
}

export interface VideoType {
  id: number;
  videoName: string;
  authorId: number;
  authorName: string;
  categoryName: string;
  highestQualityFormat: string;
  releaseDate: string;
}

export interface ProcessedVideo {
  id: number;
  name: string;
  author: string;
  categories: string[];
}

export interface VideoFormPayload {
  videoName: string;
  authorId: number;
  categoryIds: number[];
}
