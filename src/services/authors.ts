import { Author } from "../common/interfaces";

export const getAuthors = (): Promise<Author[]> => {
  return fetch(`${process.env.REACT_APP_API}/authors`).then(
    (response) => (response.json() as unknown) as Author[]
  );
};

export const editAuthor = (author: Author): Promise<Author[]> => {
  return fetch(`${process.env.REACT_APP_API}/authors/${author.id}`, {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(author),
  }).then((response) => (response.json() as unknown) as Author[]);
};
