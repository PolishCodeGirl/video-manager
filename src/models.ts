import { Author, AuthorVideoType } from "./common/interfaces";

export function VideoModel(name: string, catIds: number[]): AuthorVideoType {
  const now = new Date();

  return {
    id: Date.now(),
    name,
    catIds,
    formats: {
      one: { res: "1080p", size: 1000 },
    },
    releaseDate: `${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`,
  };
}

export const addVideoToAuthor = (author: Author, video: AuthorVideoType) => {
  return { ...author, videos: [...author.videos, video] };
};

export const editVideoInAuthor = (
  author: Author,
  editedVideo: AuthorVideoType
) => {
  return {
    ...author,
    videos: author.videos.map((currentVideo: any) =>
      currentVideo.id === editedVideo.id ? editedVideo : currentVideo
    ),
  };
};

export const removeVideoFromAuthor = (author: Author, videoId: number) => {
  return {
    ...author,
    videos: author.videos.filter(({ id }: { id: number }) => id !== videoId),
  };
};
